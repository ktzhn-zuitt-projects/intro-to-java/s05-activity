package com.zuitt;

import java.util.ArrayList;

public class Contact {

//    variables
    private String name;
    private ArrayList<String> numbers = new ArrayList<String>();
    private ArrayList<String> addresses = new ArrayList<String>();


//    default
    public Contact() {

    }

//    parameterized
    public Contact (String name, ArrayList<String> numbers, ArrayList<String> addresses) {
        this.name = name;
        this.numbers = numbers;
        this.addresses = addresses;
    }

    // Getter
    public String getName() {
        return name;
    }
    public ArrayList<String> getNumbers() {
        return numbers;
    }
    public ArrayList<String> getAddresses() {
        return addresses;
    }


    // Setter
    public void setName(String name) {
        this.name = name;
    }
    public void addNumber(String number) {
        numbers.add(String.valueOf(number));
    }
    public void addAddress(String address) {
        addresses.add(String.valueOf(address));
    }



}
