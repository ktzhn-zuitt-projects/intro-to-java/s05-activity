package com.zuitt;


public class Main {

public static void main(String[] args){
//    initialize empty phonebook
    Phonebook phonebook = new Phonebook();

//    creation of new contacts:
    Contact firstContact = new Contact();
    firstContact.setName("Reginald Cruz");
    firstContact.addNumber("63193756283");
    firstContact.addNumber("63124264926");
    firstContact.addAddress("Manila");
    firstContact.addAddress("Caloocan City");


    Contact secondContact = new Contact();
    secondContact.setName("Kris Sagabaen");
    secondContact.addNumber("633273589273");
    secondContact.addNumber("639215279267");
    secondContact.addAddress("Pasay City");
    secondContact.addAddress("Pasig City");
    secondContact.addAddress("Baguio City");


    phonebook.setContacts(firstContact);
    phonebook.setContacts(secondContact);


    if (phonebook.getContact() == null) {
        System.out.println("No entries found.");
    } else {
        for (var i = 0; i < phonebook.getContact().toArray().length; i++ ) {
            System.out.println(phonebook.getContact().get(i).getName());
            System.out.println("--------------------");
            System.out.println(phonebook.getContact().get(i).getName() + " has the following registered numbers:");
            for(var j = 0; j < phonebook.getContact().get(i).getNumbers().toArray().length; j++){
                System.out.println(phonebook.getContact().get(i).getNumbers().toArray()[j]);
            }
            System.out.println("---------------------------------");
            System.out.println(phonebook.getContact().get(i).getName() + " has the following registered addresses:");
            for(var k = 0; k < phonebook.getContact().get(i).getAddresses().toArray().length; k++){
                System.out.println(phonebook.getContact().get(i).getAddresses().toArray()[k]);
            }
            System.out.println("=================================");
        }
    }
}

}