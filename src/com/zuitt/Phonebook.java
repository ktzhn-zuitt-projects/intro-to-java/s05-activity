package com.zuitt;

import java.util.ArrayList;

public class Phonebook extends Contact {

//    variable
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    //    default
    public Phonebook() {
        super();
    }

    //    parameterized
    public Phonebook (ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

//    get
    public ArrayList<Contact> getContact() {
        return contacts;
    }
//    set
    public void setContacts(Contact contact) {
        contacts.add(contact);
    }

}
